/**
 * @file parallax_effects.js
 *   Initialize the Parallax Effect module, and thus the Skrollr library
 */

Drupal.behaviors.parallax_effects = {
  attach: function (context) {
    if (typeof skrollr != 'undefined') {
      // Initialize Skrollr
      var s = skrollr.init({
        'smoothScrolling' : Drupal.settings.parallax_effects.smoothing,
        'forceHeight' : Drupal.settings.parallax_effects.force_height,
        'scale' : Drupal.settings.parallax_effects.scale
      });

      // Initialize Skrollr Menu
      if (typeof skrollr.menu != 'undefined') {
        skrollr.menu.init(s);
      }

      // Refresh skrollr after all images have loaded, because image heights may be pertinent.
      jQuery(window).load(function() {
        s.refresh();
      });
    }
  }
};
