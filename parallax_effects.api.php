<?php

/**
 * Parallax Effects
 * Exposes a method for adding parallax effects to any page.
 *
 * PHP version 5
 *
 * @file skrollr.admin.inc
 */

/**
 * Add all applicable/configured javascript to the current page.
 *
 * @param bool $every_page Indicates that Parallax effects are not limited to pages or node types.
 */
function parallax_effects_add_js($every_page = FALSE) {
  $js_added = & drupal_static(__FUNCTION__);

  if (!$js_added) {
    $path = drupal_get_path('module', 'parallax_effects');
    $options = array(
      'scope' => 'footer',
      'group' => JS_DEFAULT,
      'every_page' => $every_page,
      'weight' => 100
    );

    // Core Skrollr library
    drupal_add_js($path . '/js/skrollr.min.js', $options);

    // Skrollr-IE
    if (variable_get('parallax_effects_ie', PARALLAX_EFFECTS_IE)) {
      $options['weight']++;
      drupal_add_js(
          $path . '/js/skrollr.ie.min.js',
          array_merge(
              $options,
              array(
                'browsers' => array(
                  'IE' => 'lt IE 9',
                  '!IE' => FALSE
                )
              )
          )
      );
    }

    // Skrollr-Stylesheets
    if (variable_get('parallax_effects_stylesheets', PARALLAX_EFFECTS_STYLESHEETS)) {
      $options['weight']++;
      drupal_add_js($path . '/js/skrollr.stylesheets.min.js', $options);
    }

    // Skrollr-Menu
    if (variable_get('parallax_effects_menu', PARALLAX_EFFECTS_MENU)) {
      $options['weight']++;
      drupal_add_js($path . '/js/skrollr.menu.min.js', $options);
    }

    // Skroll-colors
    if (variable_get('parallax_effects_colors', PARALLAX_EFFECTS_COLORS)) {
      $options['weight']++;
      drupal_add_js($path . '/js/skroll-colors.min.js', $options);
    }

    // Add settings
    drupal_add_js(
        array(
          'parallax_effects' => array(
            'smoothing' => variable_get('parallax_effects_smoothing', PARALLAX_EFFECTS_SMOOTHING) ? TRUE : FALSE,
            'force_height' => variable_get('parallax_effects_force_height', PARALLAX_EFFECTS_FORCE_HEIGHT) ? TRUE : FALSE,
            'scale' => intval(variable_get('parallax_effects_scale', PARALLAX_EFFECTS_SCALE)),
          )
        ),
        'setting'
    );

    // Init the libraries
    $options['weight']++;
    drupal_add_js($path . '/js/parallax_effects.js', $options);
  }

  $js_added = TRUE;
}