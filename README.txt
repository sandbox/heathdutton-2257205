CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Usage
 * Programmatic Usage
 * Configuration
 * Supported Skrollr plugins
 * Javascript included


INTRODUCTION
------------

Current Maintainer: heath Dutton <heathdutton@gmail.com>

There is a growing need for our clients to have various "Parallax Effects" added to existing Drupal sites.
This has been traditionally done at the theme level, ad-hoc, as needed.
Unfortunately that has resulted in ugly implementations that become tedious for developers
that may inherit a project down the line.

This module attempts to make the delivery of parallax effects as simple as possible,
using Skrollr, and Skrollr-stylesheets.

This will allow you to add any number of cool "Parallax Effects" to your theme by simply
modifying your CSS. This keeps "design" in "design" where it belongs, and out of your markup and javascript.

This module is based entirely on Prinzhorn's amazing Skrollr project:
https://github.com/Prinzhorn/skrollr

Why did I name this "Parallax Effects" instead of "Skrollr"?
1. There was already a Skrollr project.
2. A future UI companion module for configuration and effects that go way beyond including javascript.
3. Many people have no idea what "Skrollr" is, even though it's the best in it's class, and I hope to make it more popular.


USAGE
-----

1. Enable the module
2. Go to: /admin/appearance/parallax
   Here you can configure which pages/nodes deploy the javascript (and other things) to make Skrollr work.
3. Edit your stylesheets to add effects.
   See examples and details here: https://raw.githubusercontent.com/Prinzhorn/skrollr-stylesheets


PROGRAMMATIC USAGE
------------------

There are 2 ways to add effects to a page programmatically:

* Hook
  Use parallax_effects_node_view_alter(&$data, $context)
  To tell Parallax Effects to be enabled on a node-level. Simply change $effects_on_this_node in $data to TRUE.
  This is useful if you have a field on your node type to determine if parallax effects should be used or not.
* API
  If hooks are too cool for you, and you just need to load the javascript based on the configuration do this:
    module_load_include('php', 'parallax_effects', 'parallax_effects.api');
    parallax_effects_add_js();

Note: These methods above include the classes, data tags, and javascript to the page,
but it's up to your stylesheets (or markup) to add actual effects to the page.


SUPPORTED SKROLLR PLUGINS
-------------------------

* Colors - Allows you to mix and match hex, rgb and hsl colors - https://github.com/FezVrasta/skrollr-colors
* IE - Internet Explorer compatibility - https://github.com/Prinzhorn/skrollr-ie
* Menu - Enable hash based navigation (smooth scrolling) - https://github.com/Prinzhorn/skrollr-menu
* Stylesheets - Allows separation of skrollr keyframes and the document by
  putting them inside your stylesheets - https://github.com/Prinzhorn/skrollr-stylesheets
  In order to use stylesheets, you must have CSS aggregation enabled.
  Note: This module adds a class to your body tag called "parallax-effects" so that you can code your stylesheets
  to degrade gracefully when Parallax Effects are not in use on a page.


JAVASCRIPT INCLUDED
-------------------

I've included these scripts instead of usage of libraries for the sake of pain-free deployment.
That may change if any scripts included incur back-porting or compatibility problems in the future.

All inclusions are GPL licensed.

By Prinzhorn:

* skrollr.min.js - https://raw.githubusercontent.com/Prinzhorn/skrollr/master/dist/skrollr.min.js
* skrollr.menu.min.js - https://raw.githubusercontent.com/Prinzhorn/skrollr-menu/master/dist/skrollr.menu.min.js
* skrollr.ie.min.js - https://raw.githubusercontent.com/Prinzhorn/skrollr-ie/master/dist/skrollr.ie.min.js
* skrollr.stylesheets.min.js - https://raw.githubusercontent.com/Prinzhorn/skrollr-stylesheets/master/dist/skrollr.stylesheets.min.js

Third parties:

* skroll-colors.min.js - https://raw.githubusercontent.com/FezVrasta/skrollr-colors/master/dist/skroll-colors.min.js
