<?php

/**
 * Parallax Effects
 * Administration menu for the Parallax Effects module.
 *
 * PHP version 5
 *
 * @file parallax_effects.admin.inc
 */

/**
 * Generate the configuration menu
 *
 * @return mixed
 */
function parallax_effects_admin_form() {

  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE
  );

  $description = 'Inserts the "skrollr-body" ID to the body tag. ' .
    'For more advanced implementations you may need to disable this and use the ID elsewhere, ' .
    'manually. See Skrollr documentation for more details.';
  $form['settings']['parallax_effects_mobile'] = array(
    '#title' => t('Enable mobile compatibility'),
    '#description' => t($description),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_mobile', PARALLAX_EFFECTS_MOBILE)
  );

  $description = 'Automatically smoothens out scrolling effects for users who' .
    'are using a traditional mouse wheel instead of a touch interface. ' .
    'This can cause undesired results. ' .
    'See Skrollr documentation on "smoothScrolling" for more details.';
  $form['settings']['parallax_effects_smoothing'] = array(
    '#title' => t('Enable smooth scrolling'),
    '#description' => t($description),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_smoothing', PARALLAX_EFFECTS_SMOOTHING)
  );

  $description = 'Make sure the document is tall enough for all effects to be ' .
    'displayed. See Skrollr documentation on "forceHeight" for more details.';
  $form['settings']['parallax_effects_force_height'] = array(
    '#title' => t('Force a minimum document height'),
    '#description' => t($description),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_force_height', PARALLAX_EFFECTS_FORCE_HEIGHT)
  );

  $description = 'If animations are too fast/slow, adjust this number. ' .
    'See Skrollr documentation on "scale" more details. Default: 1';
  $form['settings']['parallax_effects_scale'] = array(
    '#title' => t('Animation scale'),
    '#description' => t($description),
    '#type' => 'textfield',
    '#default_value' => variable_get('parallax_effects_scale', PARALLAX_EFFECTS_SCALE)
  );

  $form['plugins'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugin Settings'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE
  );


  $form['plugins']['parallax_effects_colors'] = array(
    '#title' => t('Enable Colors plugin'),
    '#description' => t('Allows you to mix and match hex, rgb and hsl colors.') . ' ' .
      l('See details on Github', 'https://github.com/FezVrasta/skrollr-colors'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_colors', PARALLAX_EFFECTS_COLORS)
  );

  $form['plugins']['parallax_effects_ie'] = array(
    '#title' => t('Enable IE plugin'),
    '#description' => t('Provides additional Skrollr support for old versions of Internet Explorer.') . ' ' .
      l('See details on Github', 'https://github.com/Prinzhorn/skrollr-ie'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_ie', PARALLAX_EFFECTS_IE)
  );

  $form['plugins']['parallax_effects_menu'] = array(
    '#title' => t('Enable Menu plugin'),
    '#description' => t('Enable hash based navigation (smooth scrolling).') . ' ' .
      l('See details on Github', 'https://github.com/Prinzhorn/skrollr-menu'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_menu', PARALLAX_EFFECTS_MENU)
  );

  $description = 'Allows separation of Skrollr keyframes and the document, ' .
    'by putting them inside your stylesheets/theme. This is highly recommended. ' .
    'This feature requires CSS aggregation because Skrollr Stylesheets ' .
    'searches the document for LINK tags.';
  $form['plugins']['parallax_effects_stylesheets'] = array(
    '#title' => t('Enable Stylesheets plugin'),
    '#description' => t($description) . ' ' .
      l('See details on Github', 'https://github.com/Prinzhorn/skrollr-stylesheets'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('parallax_effects_stylesheets', PARALLAX_EFFECTS_STYLESHEETS)
  );

  $form['usage'] = array(
    '#type' => 'fieldset',
    '#title' => t('Usage Settings'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE
  );

  $form['usage']['parallax_effects_usage'] = array(
    '#type' => 'radios',
    '#title' => t('Use Parallax Effects on specific pages'),
    '#options' => array(
      PARALLAX_EFFECTS_NOT_LISTED => t('All pages except those listed'),
      PARALLAX_EFFECTS_LISTED => t('Only the listed pages'),
    ),
    '#default_value' => variable_get('parallax_effects_usage', PARALLAX_EFFECTS_NOT_LISTED)
  );

  $description = "Specify pages by using their paths. ' .
    'Enter one path per line. The '*' character is a wildcard. ' .
    'Example paths are %blog for the blog page and %blog-wildcard ' .
    'for every personal blog. %front is the front page.";
  $form['usage']['parallax_effects_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t(
        $description,
        array(
          '%blog' => 'blog',
          '%blog-wildcard' => 'blog/*',
          '%front' => '<front>'
        )
    ),
    '#default_value' => variable_get('parallax_effects_pages', PARALLAX_EFFECTS_PAGES)
  );

  $form['usage']['parallax_effects_node_limit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Limit Parallax Effects to specific node types'),
    '#default_value' => variable_get('parallax_effects_node_limit', PARALLAX_EFFECTS_NODE_LIMIT)
  );

  $node_types = node_type_get_types();
  $node_options = array();
  foreach ($node_types as $key => $node_type) {
    $node_options[$key] = $node_type->name;
  }
  ksort($node_options);
  $form['usage']['parallax_effects_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types to allow Parallax Effects'),
    '#options' => $node_options,
    '#default_value' => variable_get('parallax_effects_nodes', array()),
    '#states' => array(
      'invisible' => array(
        ':input[name="parallax_effects_node_limit"]' => array('checked' => FALSE),
      ),
    )
  );

  // Add a warning above the form if CSS Aggregation is not enabled
  $description = 'The Drupal feature "Aggregate and compress CSS files" ' .
    'must be enabled for Parallax Effects to work. ' .
    'Enable it <a href="%link">here</a>.';
  $preprocess_css = variable_get('preprocess_css', 0);
  if (!$preprocess_css) {
    drupal_set_message(t($description, array('%link' => '/admin/config/development/performance')), 'error');
  }

  return system_settings_form($form);
}